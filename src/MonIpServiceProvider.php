<?php

namespace Ifornew\MonIp;

use Illuminate\Support\ServiceProvider;

class MonIpServiceProvider extends ServiceProvider
{
	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$configPath = __DIR__ . '/../config/monip.php';
		if (function_exists('config_path')) {
			$publishPath = config_path('monip.php');
		} else {
			$publishPath = base_path('config/monip.php');
		}
		$this->publishes([$configPath => $publishPath], 'config');
	}

	/**
	 * Register the services providers.
	 *
	 * @return void
	 */
	public function register()
	{
		$configPath = __DIR__ . '/../config/monip.php';
		$this->mergeConfigFrom($configPath, 'monip');

		$this->app->singleton('monip', function ($app) {
			return new MonIp();
		});
		$this->app->alias('monip', MonIp::class);
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return ['monip'];
	}
}
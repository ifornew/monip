<?php

namespace Ifornew\MonIp\Facades;

use Illuminate\Support\Facades\Facade;

class MonIp extends Facade
{
	/**
	 * Get the registered name of the component.
	 *
	 * @return string
	 */
	protected static function getFacadeAccessor()
	{
		return 'monip';
	}
}
# Monip

<p align="center">
    <a href="https://packagist.org/packages/iwannamaybe/phpcas">
        <img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status">
    </a>
    <a href="https://packagist.org/packages/iwannamaybe/phpcas">
        <img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License">
    </a>
</p>

## About Monip
Monip client for the Laravel Framework 5.5+.

## Author
Ifornew

## Usage
reserve an ip to address like this:

    \Ifornew\MonIp::find('1.1.1.1');

## License
The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).